Note to self: this repo contains multiple "stow packages." The simplest cases are the `base` and `galadriel` packages,
which are intended to be installed to a user's home directory. Below is an example of how to use `stow` to deploy those
configs.

```
# In this example, I've just added whipper.conf to this repo,
# and want to deploy it to my home directory.
$ pwd
/home/josh/dotfiles
# First, do a dry run (--no), and show what would happen (-v)
$ stow --no -v galadriel/
LINK: .config/whipper/whipper.conf => ../../dotfiles/galadriel/.config/whipper/whipper.conf
WARNING: in simulation mode so not modifying filesystem.
# Note that the default installation target is the parent of the current directory,
# so the links named above will be created in /home/josh.
# Dry run output looks good: do it for real
$ stow galadriel/
$ file ~/.config/whipper/whipper.conf
/home/josh/.config/whipper/whipper.conf: symbolic link to ../../dotfiles/galadriel/.config/whipper/whipper.conf
```

Packages ending in `-etc` are, unsurprisingly, intended to be installed to `/etc`. To do so, specify the `--target`
option; for example:

```
$ hostname; pwd
treebeard
/home/josh/dotfiles
# sudo required to edit /etc
$ sudo stow --target /etc treebeard-etc/
```

For more details, see, e.g., [this blog post](https://web.archive.org/web/20221207210443/https://alexpearce.me/2016/02/managing-dotfiles-with-stow/).
